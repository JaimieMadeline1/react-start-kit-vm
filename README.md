# VM React Start Kit
Link to startkit: https://bitbucket.org/JaimieMadeline1/react-start-kit-vm/src/master/ 
Main Landing Page is within src/screens/Home/Home.js

Keywords: Stylus, CSS Modules, Webpack, Redux, Routing, Yarn, Babel, React, VSCode library modifications(Manta's Stylus Supremacy and Prettier).

VS Code Libraries to download:
- language-stylus (understanding stylus files)
- Manta's Stylus Supremacy (formatting stylus files)
- Prettier (code formatting)

CMD lines in order to start project:
- yarn install
- yarn start

Deploy:
- In index.html change lines 56 and 57 or src="/bundle.js" into src="/dist/bundle.js"
- yarn build
- yarn deploy