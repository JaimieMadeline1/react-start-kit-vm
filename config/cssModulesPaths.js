
const modules = 'node_modules';
const globalStyles = 'src/styles';

const cssModuleExlude = [
    modules,
    globalStyles
];

const stylusInclude = [
    globalStyles
];

module.exports = {
    cssModuleExlude,
    stylusInclude
}
