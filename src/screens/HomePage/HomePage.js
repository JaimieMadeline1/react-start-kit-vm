import React, { Component } from "react";

import styles from "./HomePage.styl";

class HomePage extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <React.Fragment>
        <div className={styles.container}>
          <div>Where Content Should Be.</div>
        </div>
      </React.Fragment>
    );
  }
}

export default HomePage;
